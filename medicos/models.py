from django.db import models

# Create your models here.

class Medico(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    dni = models.CharField(max_length=8)
    direccion = models.CharField(max_length=50)
    telefono = models.CharField(max_length=50)
    correo = models.EmailField(max_length=50)
    sexo = models.CharField(max_length=50)
    num_colegiatura = models.IntegerField()
    fecha_nacimiento = models.DateField()
    fecha_registro = models.DateField()
    fecha_modificacion = models.DateField()
    usuario_registro = models.CharField(max_length=50)
    usuario_modificacion = models.CharField(max_length=50)
    activo = models.BooleanField()

    def __str__(self):
        return f'{self.nombre} {self.apellido}'
    

class Especialidad(models.Model):
    nombre_especialidad = models.CharField(max_length=50)
    descripcion = models.TextField()
    fecha_registro = models.DateField()
    fecha_modificacion = models.DateField()
    usuario_registro = models.CharField(max_length=50)
    usuario_modificacion = models.CharField(max_length=50)
    activo = models.BooleanField()

    def __str__(self):
        return f'{self.nombre_especialidad}'


class Medico_Especialidad(models.Model):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidad_id = models.ForeignKey(Especialidad, on_delete=models.CASCADE)
    fecha_registro = models.DateField()
    fecha_modificacion = models.DateField()
    usuario_registro = models.CharField(max_length=50)
    usuario_modificacion = models.CharField(max_length=50)
    activo = models.BooleanField()

    


