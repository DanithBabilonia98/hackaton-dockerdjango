from rest_framework.serializers import ModelSerializer
from .models import Medico, Especialidad, Medico_Especialidad


class MedicoSerializer(ModelSerializer):
    class Meta:
        model = Medico
        fields = '__all__'
        #fields = ('id', 'nombre', 'apellido', 'telefono', 'especialidad')


class EspecialidadSerializer(ModelSerializer):
    class Meta:
        model = Especialidad
        fields = '__all__'
        #fields = ('id', 'nombre')


class MedicoEspecialidadSerializer(ModelSerializer): 
    class Meta:
        model = Medico_Especialidad
        fields = '__all__'
