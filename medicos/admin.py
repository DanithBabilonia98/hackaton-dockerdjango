from django.contrib import admin

from medicos.models import Especialidad, Medico, Medico_Especialidad

# Register your models here.

admin.site.register(Medico)
admin.site.register(Especialidad)
admin.site.register(Medico_Especialidad)