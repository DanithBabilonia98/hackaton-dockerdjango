from rest_framework import viewsets, permissions

from medicos.models import Medico, Especialidad, Medico_Especialidad

from medicos.serializers import EspecialidadSerializer, MedicoEspecialidadSerializer, MedicoSerializer

# Create your views here.

class MedicoViewSet(viewsets.ModelViewSet):
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer
    permission_classes = [permissions.IsAuthenticated]


class EspecialidadViewSet(viewsets.ModelViewSet): 
    queryset = Especialidad.objects.all()
    serializer_class = EspecialidadSerializer


class MedicoEspecialidadViewSet(viewsets.ModelViewSet):
    queryset = Medico_Especialidad.objects.all()
    serializer_class = MedicoEspecialidadSerializer