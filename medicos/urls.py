from django.urls import path, include

from rest_framework.routers import SimpleRouter
from .views import MedicoViewSet, EspecialidadViewSet, MedicoEspecialidadViewSet

router = SimpleRouter()
router.register('medicos', MedicoViewSet)
router.register('especialidades', EspecialidadViewSet)
router.register('medico_especialidades', MedicoEspecialidadViewSet)

urlpatterns = [
    path('', include(router.urls)),
]