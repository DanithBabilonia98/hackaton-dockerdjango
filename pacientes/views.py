from django.shortcuts import render
from pacientes.models import Paciente
from pacientes.serializers import PacienteSerializer

from rest_framework import viewsets

# Create your views here.

class PacienteViewSet(viewsets.ModelViewSet):
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer