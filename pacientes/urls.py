from django.urls import path, include
from pacientes.views import PacienteViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('pacientes', PacienteViewSet)

urlpatterns = [
    path('', include(router.urls)),
]