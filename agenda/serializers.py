from rest_framework.serializers import ModelSerializer
from .models import Horario, Cita


class CitaSerializer(ModelSerializer):
    class Meta:
        model = Cita
        fields = '__all__'

class HorarioSerializer(ModelSerializer):
    class Meta:
        model = Horario
        fields = '__all__'