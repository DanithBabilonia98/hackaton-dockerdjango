from django.shortcuts import render
from rest_framework import viewsets, permissions

from agenda.models import Horario, Cita
from agenda.serializers import HorarioSerializer, CitaSerializer

# Create your views here.

class HorarioViewSet(viewsets.ModelViewSet):
    queryset = Horario.objects.all()
    serializer_class = HorarioSerializer
    
class CitaViewSet(viewsets.ModelViewSet):
    queryset = Cita.objects.all()
    serializer_class = CitaSerializer