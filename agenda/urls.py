from django.urls import path, include
from agenda.views import CitaViewSet, HorarioViewSet
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register('horarios',HorarioViewSet)
router.register('citas',CitaViewSet)


urlpatterns = [
    path('',include(router.urls))
]