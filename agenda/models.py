from django.db import models

from medicos.models import Medico
from pacientes.models import Paciente

# Create your models here.

class Horario(models.Model):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    hora_inicio = models.TimeField()
    hora_fin = models.TimeField()
    fecha_registro = models.DateField()
    fecha_modificacion = models.DateField()
    usuario_registro = models.CharField(max_length=50)
    usuario_modificacion = models.CharField(max_length=50)
    activo = models.BooleanField()


class Cita(models.Model):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    paciente_id = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    fin_atencion = models.TimeField()
    estado = models.CharField(max_length=50)
    observacion = models.TextField()
    fecha_registro = models.DateField()
    fecha_modificacion = models.DateField()
    usuario_registro = models.CharField(max_length=50)
    usuario_modificacion = models.CharField(max_length=50)
    activo = models.BooleanField()

    def __str__(self):
        return f'{self.medico_id} - {self.paciente_id}'