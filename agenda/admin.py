from django.contrib import admin

from agenda.models import Cita, Horario

# Register your models here.

admin.site.register(Horario)
admin.site.register(Cita)